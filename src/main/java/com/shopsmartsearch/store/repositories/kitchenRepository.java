package com.shopsmartsearch.store.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopsmartsearch.store.entities.Kitchen;
@Repository
public interface kitchenRepository extends JpaRepository<Kitchen, Long> {

}
