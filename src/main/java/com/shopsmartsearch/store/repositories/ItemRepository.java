package com.shopsmartsearch.store.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopsmartsearch.store.entities.Item;
@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

}
