package com.shopsmartsearch.store.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.annotation.Transient;

@Entity
public class CustomerOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String deliveryAddress;
	private String status;
	private LocalDateTime orderDateTime;
	private String addOn;
	private double totalPrice;
	private String paymentMethod;
	private boolean confirmed;

	@ManyToOne
	private User user;

	@OneToMany(mappedBy = "customerOrder", cascade = CascadeType.ALL)
	@Transient
	private List<Item> items = new ArrayList<>();

	@Transient
	@ManyToMany
	@JoinTable(name = "Kitchens_customerOrders", joinColumns = @JoinColumn(name = "customerOrder_id"), inverseJoinColumns = @JoinColumn(name = "kitchen_id"))
	private List<Kitchen> kitchens;

	
	public List<Kitchen> getKitchens() {
		return kitchens;
	}

	public void setKitchens(List<Kitchen> kitchens) {
		this.kitchens = kitchens;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getOrderDateTime() {
		return orderDateTime;
	}

	public void setOrderDateTime(LocalDateTime orderDateTime) {
		this.orderDateTime = orderDateTime;
	}

	public String getAddOn() {
		return addOn;
	}

	public void setAddOn(String addOn) {
		this.addOn = addOn;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public CustomerOrder() {
		super();
		// TODO Auto-generated constructor stub
	}

}
