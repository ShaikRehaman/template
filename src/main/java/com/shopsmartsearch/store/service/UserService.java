package com.shopsmartsearch.store.service;

import java.util.List;

import com.shopsmartsearch.store.entities.User;

public interface UserService {
	List<User>getAllUsers();
	User createUser(User user);

}
