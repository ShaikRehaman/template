package com.shopsmartsearch.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;




@SpringBootApplication //(exclude = {SecurityAutoConfiguration.class})
@EnableSwagger2WebMvc
@Import(SpringDataRestConfiguration.class) 
public class StoreApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(StoreApplication.class, args);
	}
	

	@Override
	  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	    return builder.sources(StoreApplication.class);
	  }
	
	
	
	/*@Bean
    public void dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mydb");
        dataSource.setUsername("sa");
        dataSource.setPassword("");


        Resource initSchema = new ClassPathResource("h2database.sql");
        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema);
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);

    }*/
	
}
